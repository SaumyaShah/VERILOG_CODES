module sub4_tb;

reg [3:0] x;
reg [3:0] y;
reg b;
wire [3:0] sum;
wire borrow;

sub_4 DUT(.x(x),.y(y),.b(b),.sum(sum),.borrow(borrow));
initial begin
	x=4'b1111;
	y=4'b1000;
	b=4'b0001;
	#10
	$display(sum);
	$display(borrow);
end
endmodule
