module sub32_tb;
reg [31:0] x;
reg [31:0] y;
reg b;
wire [31:0] sum;
wire [0:0] borrow;

sub_32 DUT(.x(x),.y(y),.b(b),.sum(sum),.borrow(borrow));

initial begin
	x=32'b10101010001;
	y=32'b00101011111;
	b=1'b1;
	#20
	$display(x);
	$display(y);
	$display(sum);
	$display(borrow);
end

endmodule
