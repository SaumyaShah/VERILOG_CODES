module jkff_tb;
        reg clk,reset,j,k;
        wire q;
    
        jkff DUT(.clk(clk),.reset(reset),.j(j),.k(k),.q(q));

        initial begin

                clk=1'b1;
                j=1'b0;
		k=1'b1;
                reset=1'b0;
		#5
		j=1'b1;
		k=1'b0;
                #20 
                reset=1'b1;
                #2
                $display(q);   //t=27 op=0
                #20
                $display(q);   //t=47 op=1
                #10
		j=1'b1;
                k=1'b1;
		#5
		$display(q);   //t=62 op=0(toggled)
		#10
		j=1'b0;
		k=1'b0;
		#10
		$display(q);// t=82 op=0(remains same)
		#10
		j=1'b1;
                k=1'b1;
                #10
                $display(q);   //t=102 op=1(toggled)
                #10
                j=1'b0;
                k=1'b0;
                #10
		$display(q);  // t=122 op=1(same)
                #10
		reset=1'b0;
                #10
                $display(q); // t=132 op=0(reset)
                #5
                $finish;
        end

        always begin
                 #10 
                 clk=~clk;
        end

	initial begin
                $dumpfile("JKFF.vcd");
                $dumpvars(0,jkff_tb);
        end

endmodule
