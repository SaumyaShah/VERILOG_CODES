module dff(clk,reset,d,q);
	input clk;
	input reset;
	input d;
	output q;

	reg q;

	always @(posedge clk or negedge reset)
	begin
		if(!reset)
			q<=1'b0;
		else
			q<=d;
	end
endmodule
