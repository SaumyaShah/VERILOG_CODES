module jkff(clk,reset,j,k,q);
        input clk;
        input reset;
        input j;
	input k;
        output q;

        reg q;

        always @(posedge clk or negedge reset)
        begin
                if(!reset)
                        q<=1'b0;
                else
                        q<=(j&(~q))|((~k)&q);
        end 
endmodule

