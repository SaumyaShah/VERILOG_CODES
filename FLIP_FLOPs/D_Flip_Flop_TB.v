module dff_tb;
	reg clk,reset,d;
	wire q;
	
	dff DUT(.clk(clk),.reset(reset),.d(d),.q(q));

	initial begin

		clk=1'b1;
		d=1'b0;
		reset=1'b0;
		#15
		reset=1'b1;
		d=1'b1;
		#20
		$display(q);
		#17
		$display(q);
		#100
		reset=1'b0;
		#2
		$display(q);
		#5 
		$finish;
		
	end

	always begin
		 #10
		 clk=~clk;
	end

endmodule
