module comp32(a,b,gt,ls,eq);
input [31:0] a;
input [31:0] b;
output gt;
output ls;
output eq;

comp16 c32_1(a[31:16],b[31:16],gt32_1,ls32_1,eq32_1);
comp16 c32_0(a[15:0],b[15:0],gt32_0,ls32_0,eq32_0);

assign gt = gt32_1+(eq32_1&gt32_0);
assign ls = ls32_1+(eq32_1&ls32_0);
assign eq = (eq32_1&eq32_0);

endmodule