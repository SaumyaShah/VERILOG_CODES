module comp2(a,b,gt,ls,eq);
input [1:0] a;
input [1:0] b;
output gt;
output ls;
output eq;

assign x0=~(a[0]^b[0]);
assign x1=~(a[1]^b[1]);

assign eq = x0&x1;
assign gt = (a[1]&(~b[1])) | (a[0]&(~b[0])&x1);
assign ls = ((~a[1])&b[1]) | ((~a[0])&b[0]&x1);

endmodule
