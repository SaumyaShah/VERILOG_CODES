module gcd_tb;
reg [31:0] a;
reg [31:0] b;
reg clk;
reg reset;
wire [31:0] out;

gcd DUT(.a(a),.b(b),.clk(clk),.reset(reset),.out(out));

initial begin
	clk=1'b0;
	reset=1'b1;
	a=32'b110;
	b=32'b1010;
	#1000
	$display(a);
	$display(b);
	$display(out);

	a=32'b10010;
	b=32'b11000;
	#1000
	$display(a);
	$display(b);
	$display(out);

	a=32'b0;
	b=32'b101;
	#1000
	$display(a);
	$display(b);
	$display(out);


	a=32'b0100111;
	b=32'b011;
	#1000
	$display(a);
	$display(b);
	$display(out);


	a=32'b1111;
	b=32'b1011;
	#1000
	$display(a);
	$display(b);
	$display(out);

	a=32'b111111111111110000011111111;
	b=32'b010010101010101010100000000;
	#1000
	$display(a);
	$display(b);
	$display(out);

	$finish;
end 

always begin
	#10 clk=~clk;
end

initial begin
    $dumpfile("gcd.vcd");
    $dumpvars(0,gcd_tb);
end


endmodule
/*initial begin
    $dumpfile("gcd.vcd");
    $dumpvars(0,gcd_tb);
end
*/