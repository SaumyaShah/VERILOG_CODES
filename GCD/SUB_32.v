module sub_32(x,y,b,sum,borrow);

input [31:0] x;
input [31:0] y;
input [0:0] b;
output [31:0] sum;
output [0:0] borrow;

/*initial begin
	$display("sub %b %b",x,y);
end*/

// Full_Adder(x,y,z,sum,carry);

/*Full_Adder fa0(x[0],1^(y[0]),1,sum[0],c0); 
Full_Adder fa1(x[1],1^(y[1]),c0,sum[1],c1);
Full_Adder fa2(x[2],1^(y[2]),c1,sum[2],c2);
Full_Adder fa3(x[3],1^(y[3]),c2,sum[3],c3);*/

// module sub_4(x,y,sum,borrow);

sub_4 sb0(x[3:0],y[3:0],b,sum[3:0],b0);
sub_4 sb1(x[7:4],y[7:4],b0,sum[7:4],b1);
sub_4 sb2(x[11:8],y[11:8],b1,sum[11:8],b2);
sub_4 sb3(x[15:12],y[15:12],b2,sum[15:12],b3);
sub_4 sb4(x[19:16],y[19:16],b3,sum[19:16],b4);
sub_4 sb5(x[23:20],y[23:20],b4,sum[23:20],b5);
sub_4 sb6(x[27:24],y[27:24],b5,sum[27:24],b6);
sub_4 sb7(x[31:28],y[31:28],b6,sum[31:28],b7);

assign borrow = b7;

endmodule
