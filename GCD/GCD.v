module gcd(a,b,clk,reset,out);
input  [31:0] a;
input  [31:0] b;
input clk;
input reset;
output reg [31:0] out;
reg [31:0] a_in;
reg [31:0] b_in;
parameter s_0=2'b0,s_1=2'b1,s_2=2'b10,s_3=2'b11;
reg [1:0] state;
wire [31:0] a_out;
wire [31:0] b_out;



always @(posedge clk && state==2'b0 && reset==1)
begin

	a_in<=a;
	b_in<=b;
end


comp32 c0(a_in,b_in,gt0,ls0,eq0);
sub_32 sa(a_in,b_in,1,a_out,borrow_a);
sub_32 sb(b_in,a_in,1,b_out,borrow_b);

always @(posedge clk)
begin

	if(reset==1'b0)
	begin
		state<=s_0;
		out <=0;

	end
	else if(a_in==32'b0) 
	begin
		out<=b_in;
		state<=s_0;
	end
	else if(b_in==32'b0)
	begin
		out<=a_in;
		state<=s_0;
	end
	else begin
		case(state)
		
		s_0:if(gt0==1)
			begin
				a_in<=a_out;
				state<=s_1;				
			end
			else if(ls0==1) 
			begin
				b_in<=b_out;
				state<=s_2;
			end
			else begin
				out<=a_in;
				state<=s_3;
			end
		
		s_1:if(gt0==1)
			begin
				a_in<=a_out;				
				state<=s_1;
			end
			else if(ls0==1) 
			begin
				b_in<=b_out;
				state<=s_2;
			end
			else begin
				out<=a_in;
				state<=s_3;
			end
		
		s_2:if(gt0==1)
			begin
				a_in<=a_out;				
				state<=s_1;
			end
			else if(ls0==1) 
			begin
				b_in<=b_out;
				state<=s_2;
			end
			else begin
				out<=a_in;
				state<=s_3;
			end
		
		default:state<=s_0;
		endcase
	end
end


endmodule
