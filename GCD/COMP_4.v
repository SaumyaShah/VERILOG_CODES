module comp4(a,b,gt,ls,eq);
input [3:0] a;
input [3:0] b;
output gt;
output ls;
output eq;

comp2 c1(a[3:2],b[3:2],gt1,ls1,eq1);
comp2 c0(a[1:0],b[1:0],gt0,ls0,eq0);

assign gt = gt1+(eq1&gt0);
assign ls = ls1+(eq1&ls0);
assign eq = (eq1&eq0);

endmodule