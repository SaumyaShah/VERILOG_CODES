module sub_4(x,y,b,sum,borrow);

input [3:0] x;
input [3:0] y;
input [0:0] b;
output [3:0] sum;
output [0:0] borrow;

// Full_Adder(x,y,z,sum,carry);

Full_Adder fa0(x[0],1^(y[0]),b,sum[0],c0); 
Full_Adder fa1(x[1],1^(y[1]),c0,sum[1],c1);
Full_Adder fa2(x[2],1^(y[2]),c1,sum[2],c2);
Full_Adder fa3(x[3],1^(y[3]),c2,sum[3],c3);

assign borrow = c3;

endmodule
