module comp8(a,b,gt,ls,eq);
input [7:0] a;
input [7:0] b;
output gt;
output ls;
output eq;

comp4 c8_1(a[7:4],b[7:4],gt8_1,ls8_1,eq8_1);
comp4 c8_0(a[3:0],b[3:0],gt8_0,ls8_0,eq8_0);

assign gt = gt8_1+(eq8_1&gt8_0);
assign ls = ls8_1+(eq8_1&ls8_0);
assign eq = (eq8_1&eq8_0);

endmodule