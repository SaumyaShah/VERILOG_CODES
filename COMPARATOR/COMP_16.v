module comp16(a,b,gt,ls,eq);
input [15:0] a;
input [15:0] b;
output gt;
output ls;
output eq;

comp8 c16_1(a[15:8],b[15:8],gt16_1,ls16_1,eq16_1);
comp8 c16_0(a[7:0],b[7:0],gt16_0,ls16_0,eq16_0);

assign gt = gt16_1+(eq16_1&gt16_0);
assign ls = ls16_1+(eq16_1&ls16_0);
assign eq = (eq16_1&eq16_0);

endmodule