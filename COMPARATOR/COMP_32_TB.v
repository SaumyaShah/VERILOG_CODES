module comp_32_tb;
reg [31:0] a;
reg [31:0] b;

wire gt,ls,eq;

comp32 DUT(.a(a),.b(b),.gt(gt),.ls(ls),.eq(eq));

initial begin
	a=32'b111010110101;
	b=32'b01010101000;
	#20
	$display(a);
	$display(b);
	$display(gt);
	$display(ls);
	$display(eq);
	a=32'b11010110101;
	b=32'b010101010101;
	#20
	$display(a);
	$display(b);
	$display(gt);
	$display(ls);
	$display(eq);
	a=32'b10101110101;
	b=32'b010100111010;
	#20
	$display(a);
	$display(b);
	$display(gt);
	$display(ls);
	$display(eq);
	a=32'b1100010100111;
	b=32'b1101010100111;
	#20
	$display(a);
	$display(b);
	$display(gt);
	$display(ls);
	$display(eq);
	a=32'b11;
	b=32'b00000000011;
	#20
	$display(a);
	$display(b);
	$display(gt);
	$display(ls);
	$display(eq);
	a=32'b101011101;
	b=32'b00101011100;
	#20
	$display(a);
	$display(b);
	$display(gt);
	$display(ls);
	$display(eq);
	a=32'b001001011;
	b=32'b01001011;
	#20
	$display(a);
	$display(b);
	$display(gt);
	$display(ls);
	$display(eq);
	a=32'b11100001011;
	b=32'b11100101011;
	#20
	$display(a);
	$display(b);
	$display(gt);
	$display(ls);
	$display(eq);


	a=32'b11;
	b=32'b1;
	#20
	$display(gt);
	$display(ls);
	$display(eq);

end

endmodule