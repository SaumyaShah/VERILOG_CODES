module comp2_tb;
reg [1:0] a;
reg [1:0] b;

wire gt;
wire lt;
wire eq;

comp2 DUT(.a(a),.b(b),.gt(gt),.lt(lt),.eq(eq));

initial begin
	a=2'b11;
	b=2'b01;
	#20
	$display(eq);
	$display(lt);
	$display(gt);

end
endmodule