module comp_8_tb;
reg [7:0] a;
reg [7:0] b;

wire gt,ls,eq;

comp8 DUT(.a(a),.b(b),.gt(gt),.ls(ls),.eq(eq));

initial begin
	a=8'b11;
	b=8'b11;
	#20
	$display(gt);
	$display(ls);
	$display(eq);

end

endmodule