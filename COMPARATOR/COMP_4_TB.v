module comp_4_tb;
reg [3:0] a;
reg [3:0] b;

wire gt,ls,eq;

comp4 DUT(.a(a),.b(b),.gt(gt),.ls(ls),.eq(eq));

initial begin
	a=4'b10;
	b=4'b11;
	#20
	$display(gt);
	$display(ls);
	$display(eq);

end

endmodule