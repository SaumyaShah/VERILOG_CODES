module mult_4bit(clk,reset,A,B,start,product,done);

//I/O
input clk;
input reset;
input [3:0] A;
input [3:0] B;
input start;
output reg  [7:0] product;
output reg done=0;
/*output reg [3:0] pp0;
output reg [3:0] pp1;
output reg [3:0] pp2;
output reg [3:0] pp3;
output wire [4:0] In1_C;
output wire [3:0] In1_S;
output wire [4:0] In2_C;
output wire [3:0] In2_S;
output wire [4:0] In3_C;
output wire [1:0] Store_pp;*/


//Converting wire input to reg
reg [3:0] A_in;
reg [3:0] B_in;
//reg start_in;
reg state=0;

reg [3:0] pp0;
reg [3:0] pp1;
reg [3:0] pp2;
reg [3:0] pp3;
wire [4:0] In1_C;
wire [3:0] In1_S;
wire [4:0] In2_C;
wire [3:0] In2_S;
wire [4:0] In3_C;
wire [1:0] Store_pp;



always @(reset==0)
begin
	A_in<=0;
	B_in<=0;
	done<=0;
	product<=0;
end

always @(posedge clk && reset==1 && state==0 && start==1)
begin
		A_in<=A;
		B_in<=B;
		state<=1;
end

//1
Half_Adder a00(pp0[1],pp1[0],p1,In1_C[0]);
Full_Adder a01(pp0[2],pp1[1],pp2[0],In1_S[0],In1_C[1]);
Full_Adder a02(pp1[2],pp2[1],pp3[0],In1_S[1],In1_C[2]);
Full_Adder a03(pp1[3],pp2[2],pp3[1],In1_S[2],In1_C[3]);
Half_Adder a04(pp2[3],pp3[2],In1_S[3],In1_C[4]);
assign Store_pp[0]=pp3[3];  //a3b3
assign Store_pp[1]=pp0[3];	//a3b0
assign p0 = pp0[0] ;

//2
Half_Adder a10(In1_S[0],In1_C[0],p2,In2_C[0]);
Full_Adder a11(Store_pp[1],In1_S[1],In1_C[1],In2_S[0],In2_C[1]);
Half_Adder a12(In1_S[2],In1_C[2],In2_S[1],In2_C[2]);
Half_Adder a13(In1_S[3],In1_C[3],In2_S[2],In2_C[3]);
Half_Adder a14(Store_pp[0],In1_C[4],In2_S[3],In2_C[4]);

//3
Half_Adder a20(In2_S[0],In2_C[0],p3,In3_C[0]);
Full_Adder a21(In2_S[1],In2_C[1],In3_C[0],p4,In3_C[1]);
Full_Adder a22(In2_S[2],In2_C[2],In3_C[1],p5,In3_C[2]);
Full_Adder a23(In2_S[3],In2_C[3],In3_C[2],p6,In3_C[3]);
Half_Adder a24(In2_C[4],In3_C[3],p7,In3_C[4]);           // In3_C[4] will be 0



always @(posedge clk && reset==1 && start==1)
begin
	if(state==1)
	begin
		pp0[0] <= A_in[0]&B_in[0];
		pp0[1] <= A_in[1]&B_in[0];
		pp0[2] <= A_in[2]&B_in[0];
		pp0[3] <= A_in[3]&B_in[0];
		pp1[0] <= A_in[0]&B_in[1];
		pp1[1] <= A_in[1]&B_in[1];
		pp1[2] <= A_in[2]&B_in[1];
		pp1[3] <= A_in[3]&B_in[1];
		pp2[0] <= A_in[0]&B_in[2];
		pp2[1] <= A_in[1]&B_in[2];
		pp2[2] <= A_in[2]&B_in[2];
		pp2[3] <= A_in[3]&B_in[2];
		pp3[0] <= A_in[0]&B_in[3];
		pp3[1] <= A_in[1]&B_in[3];
		pp3[2] <= A_in[2]&B_in[3];
		pp3[3] <= A_in[3]&B_in[3];
		
		product[0]<=p0;
		product[1]<=p1;
		product[2]<=p2;
		product[3]<=p3;
		product[4]<=p4;
		product[5]<=p5;
		product[6]<=p6;
		product[7]<=p7;
		state<=0;
		done=1;
	end
end

endmodule
