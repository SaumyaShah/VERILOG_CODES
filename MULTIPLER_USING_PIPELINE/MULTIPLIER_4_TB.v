module mult_4bit_TB;
reg [3:0] A;
reg [3:0] B;
reg clk;
reg reset;
reg start;
wire done;
wire [7:0] product;
/*wire [3:0] pp0;
wire [3:0] pp1;
wire [3:0] pp2;
wire [3:0] pp3;
wire [4:0] In1_C;
wire [3:0] In1_S;
wire [4:0] In2_C;
wire [3:0] In2_S;
wire [4:0] In3_C;
*/

mult_4bit DUT(.clk(clk),.reset(reset),.A(A),.B(B),.start(start),.product(product),.done(done));

initial begin
	A=4'b1111;
	B=4'b1111;
	start=1;
	reset=0;
	clk=1;
	#2
	reset=1;
	#1000
	/*$display(A);
	$display(B);
	$display(product);
	$display(done);*/

	/*$display("%b",pp0);
	$display(pp1);
	$display(pp2);
	$display(pp3);*/
	$display(A);
	$display(B);
	$display("%d",product);
	$display(done);
	/*$display("In1_S=%b",In1_S);
	$display("In1_C=%b",In1_C);
	$display("In2_S=%b",In2_S);
	$display("In2_C=%b",In2_C);
	$display("In3_C=%b",In3_C);
	$display(done);
*/
	#20
	reset=0;
	#10
	reset=1;
	#100
	start=0;
	A=4'b1101;
	B=4'b1001;
	#1000
	/*$display(A);
	$display(B);
	$display(product);*/
/*	$display("%b",pp0);
	$display(pp1);
	$display(pp2);
	$display(pp3);
*/	$display(A);
	$display(B);
	$display("%d",product);
	$display(done);
/*	$display("In1_S=%b",In1_S);
	$display("In1_C=%b",In1_C);
	$display("In2_S=%b",In2_S);
	$display("In2_C=%b",In2_C);
	$display("In3_C=%b",In3_C);
	$display(done);*/
	start=1;
	A=4'b0101;
	B=4'b0000;
	#1000
	$display(A);
	$display(B);
	$display("%d",product);
	$display(done);

	start=1;
	A=4'b1110;
	B=4'b0110;
	#1000
	$display(A);
	$display(B);
	$display("%d",product);
	$display(done);

	start=1;
	reset=0;
	A=4'b1010;
	B=4'b0110;
	#1000
	$display(A);
	$display(B);
	$display("%d",product);
	$display(done);

	reset=1;
	start=1;
	A=4'b1111;
	B=4'b0110;
	#1000
	$display(A);
	$display(B);
	$display("%d",product);
	$display(done);
	

	$finish;
end

always #100 clk=~clk;
initial begin
    $dumpfile("mult_4bit.vcd");
    $dumpvars(0,mult_4bit_TB);
end
endmodule