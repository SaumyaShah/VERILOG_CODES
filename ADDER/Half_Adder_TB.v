module Half_Adder_tb;
	reg x,y;
	wire s,c;
	Half_Adder DUT(.x(x),.y(y),.s(s),.c(c));

	initial begin
		x=0;
		y=0;
		#20;
		$display(s);
		$display(c);
		x=0;
                y=1;
                #20;
                $display(s);
                $display(c);
		x=1;
                y=0;
                #20;
                $display(s);
                $display(c);
		x=1;
                y=1;
                #20;
                $display(s);
                $display(c);
		$finish;
	end
	
	initial begin
                $dumpfile("ha.vcd");
                $dumpvars(0,Half_Adder_tb);
        end

endmodule
