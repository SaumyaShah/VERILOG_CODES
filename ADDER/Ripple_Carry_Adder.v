module Ripple_Carry_Adder(in1,in2,cin,fadd);
input [3:0] in1;
input [3:0] in2;
input cin;
output [4:0] fadd;
wire [3:0] addition;
output fcarry;

Full_Adder fa0(in1[0],in2[0],cin,addition[0],c0);
Full_Adder fa1(in1[1],in2[1],c0,addition[1],c1);
Full_Adder fa2(in1[2],in2[2],c1,addition[2],c2);
Full_Adder fa3(in1[3],in2[3],c2,addition[3],c3);

assign fadd={c3,addition};

endmodule
