module Full_Adder_tb;

	reg x,y,z;
	wire sum,carry;

	Full_Adder DUT(.x(x),.y(y),.z(z),.sum(sum),.carry(carry));
	
	initial begin
		x=0;
		y=0;
		z=0;
		#20
		$display(sum);
		$display(carry);
		x=0;
                y=0;
                z=1;
                #20
                $display(sum);
                $display(carry);
		x=0;
                y=1;
                z=0;
                #20
                $display(sum);
                $display(carry);
		x=0;
                y=1;
                z=1;
                #20
                $display(sum);
                $display(carry);
		x=1;
                y=0;
                z=0;
                #20
                $display(sum);
                $display(carry);
		x=1;
                y=0;
                z=1;
                #20
                $display(sum);
                $display(carry);
		x=1;
                y=1;
                z=0;
                #20
                $display(sum);
                $display(carry);
		x=1;
                y=1;
                z=1;
                #20
                $display(sum);
                $display(carry);
		$finish;
	end

	 initial begin
                $dumpfile("fa.vcd");
                $dumpvars(0,Full_Adder_tb);
        end
endmodule
