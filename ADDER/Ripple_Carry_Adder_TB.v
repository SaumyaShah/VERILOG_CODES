module Ripple_Carry_Adder_tb;

	reg [3:0] in1;
	reg [3:0] in2;
	reg cin;

	wire [4:0] fadd;

	Ripple_Carry_Adder DUT(.in1(in1),.in2(in2),.cin(cin),.fadd(fadd));
	initial begin
	
		in1=4'b1001;
		in2=4'b1000;
		cin=0;
		#20;
		$display(fadd);

		
		in1=4'b1111;
                in2=4'b1111;
		cin=0;
		#20
		$display(fadd);


		in1=4'b0000;
                in2=4'b1011;
		cin=0;
		#20
		$display(fadd);


		in1=4'b1000;
                in2=4'b1100;
		cin=0;
		#20
		$display(fadd);


		in1=4'b0101;
                in2=4'b1010;
		cin=0;
                #20;
		$display(fadd);
		$finish;

	end
	initial begin
                $dumpfile("rca.vcd");
                $dumpvars(0,Ripple_Carry_Adder_tb);
        end

endmodule
 
